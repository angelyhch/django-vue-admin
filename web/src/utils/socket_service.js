export default class SocketService {
  /**
   * 单例模式
   */

  static instance = null;
  static get Instance() {
    if (!this.instance) {
      this.instance = new SocketService();
    }
    return this.instance;
  }

  ws = null;
  connected = false;
  // 记录重新发送尝试的次数
  sendRetryCount = 0;

  // 记录重新连接的次数
  connectRetryCount = 0;

  // 存储回调函数映射
  callBackMapping = {};

  // 定义连接服务器的方法
  connect() {
    // 连接服务器
    if (!window.WebSocket) {
      console.log("您的浏览器不支持WebSocket");
      return;
    }
    const backendHost = window.location.hostname;
    this.ws = new WebSocket("ws://" + backendHost + ":9998");

    // 连接成功的事件监听
    this.ws.onopen = () => {
      console.log("连接服务端成功");
      this.connected = true;
      this.connectRetryCount = 0;
    };

    // 1.连接服务端失败
    // 2.连接成功后,服务器关闭时候也会触发调用,在此处增加重连的尝试
    this.ws.onclose = () => {
      console.log("连接服务端失败");
      this.connected = false;
      this.connectRetryCount++;

      setTimeout(() => {
        this.connect();
      }, this.connectRetryCount * 500);
    };

    // 得到服务端发送过来的参数
    this.ws.onmessage = (msg) => {
      console.log("从服务端获取到了数据");

      // 真正服务端发送过来的数据,放在msg.data中
      // console.log(msg.data);
      // console.log(this.callBackMapping[socketType]);

      const recvData = JSON.parse(msg.data);
      const socketType = recvData.socketType;

      // 判断回调函数是否存在
      if (this.callBackMapping[socketType]) {
        const action = recvData.action;
        if (action === "getData") {
          const realData = JSON.parse(recvData.data);
          // console.log(realData);
          // TODO:待理解
          this.callBackMapping[socketType].call(this, realData);
        } else if (action === "fullScreen") {
          this.callBackMapping[socketType].call(this, recvData);
        } else if (action === "changeTheme") {
        }
      }
    };
  }

  // 注册回调函数
  registerCallBack(socketType, callBack) {
    this.callBackMapping[socketType] = callBack;
  }

  // 取消回调函数
  unregisterCallBack(socketType) {
    this.callBackMapping[socketType] = null;
  }

  // 发送数据的方法
  send(data) {
    // 判断是否已经连接成功
    if (this.connected) {
      this.ws.send(JSON.stringify(data));
    } else {
      this.sendRetryCount++;
      setTimeout(() => {
        this.ws.send(JSON.stringify(data));
      }, this.sendRetryCount * 500);
    }
  }
}
