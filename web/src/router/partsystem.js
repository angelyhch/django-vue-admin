import PartScreen from "@/views/partsystem/ScreenPage";
export default [
  {
    path: "/part/screen",
    name: "screen",
    component: PartScreen,
  },
];
