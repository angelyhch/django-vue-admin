from django.apps import AppConfig


class Chart01Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chart01'
