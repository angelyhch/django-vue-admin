from base64 import encode
from ctypes import HRESULT
from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import json
from django.conf import settings
import os


# Create your views here.


def home(request):
    return HttpResponse('hello chart')


def readData():
    with open(os.path.join(settings.BASE_DIR, 'chart01/data/rank.json'), 'r', encoding='utf-8') as f:
        data1 = json.loads(f.read())
        print(data1)

# readData()


def dataForChart01(request):
    with open(os.path.join(settings.BASE_DIR, 'chart01/data/rank.json'), 'r', encoding='utf-8') as f:
        data0 = f.read()
        print(data0)

        return JsonResponse(data0, safe=False)

        # return HttpResponse(data0)
