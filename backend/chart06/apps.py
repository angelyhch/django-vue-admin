from django.apps import AppConfig


class Chart06Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chart06'
