from django.apps import AppConfig


class Chart05Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chart05'
