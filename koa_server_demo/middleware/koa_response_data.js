// // 处理业务逻辑的中间件,读取某个json文件的数据
// const path = require('path')
// const fileUtils = require('../utils/file_utils')
// module.exports = async (ctx, next) => {
//   // 根据url
//   const url = ctx.request.url // /api/seller   ../data/seller.json
//   let filePath = url.replace('/api', '') //  /seller
//   filePath = '../data' + filePath + '.json'  // ../data/seller.json
//   filePath = path.join(__dirname, filePath)
//   try {
//     const ret = await fileUtils.getFileJsonData(filePath)
//     ctx.response.body = ret
//   } catch (error) {
//     const errorMsg = {
//       message: '读取文件内容失败, 文件资源不存在',
//       status: 404
//     }
//     ctx.response.body = JSON.stringify(errorMsg)
//   }

//   console.log(filePath)
//   await next()
// }

const path = require('path')
module.exports = async (ctx, next) => {
  const url = ctx.request.url // /api/seller
  // console.log(url)

  let filePath = url.replace('/api', '')
  filePath = '../data' + filePath + '.json'
  filePath = path.join(__dirname, filePath)
  // console.log(filePath)

  // 导入file读取工具
  const fileUtils = require('../utils/file_utils')

  try {
    const ret = await fileUtils.getFileJsonData(filePath)
    // console.log('ret@@', ret)
    ctx.response.body = ret
  } catch {
    console.log('文件读取失败!请检查路径是否正确?')
  }
  // 虽然是最内存的中间件,但是建议也加上next,因为别人可能没有用在最内层
  await next()
}
