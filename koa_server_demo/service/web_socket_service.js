// 引入websocket包
const WebSocket = require('ws')

// 创建WebSocket 服务端的对象,绑定 端口号是9998
const wss = new WebSocket.Server({
  port: 9998
})
const fileUtils = require('../utils/file_utils')
const path = require('path')
module.exports.listen = () => {
  // 对客户端的连接事件进行监听
  wss.on('connection', (client) => {
    console.log('有客户端连接成功了...')
    // 对客户端的连接对象进行message事件的监听
    // 是客户端发送给服务端的数据
    client.on('message', async (msg) => {
      console.log('客户端发送数据给服务端了...:', msg)
      let payload = JSON.parse(msg)
      const action = payload.action
      if (action === 'getData') {
        // payload.charName // trend seller map rank hot stock
        let filePath = '../data/' + payload.chartName + '.json'
        filePath = path.join(__dirname, filePath) // 绝对路径

        const ret = await fileUtils.getFileJsonData(filePath)

        // console.log(ret)
        // 在服务器获取到的数据添加data
        payload.data = ret
        client.send(JSON.stringify(payload))
      } else {
        // 原封不动的将所接受的数据发给每一个处于连接的客户端
        // wss.clients  //所有客户端
        recvData = JSON.parse(msg)
        // console.log(recvData)
        // console.log(msg)
        wss.clients.forEach((client) => {
          client.send(JSON.stringify(recvData))
        })
      }

      // 由客户端发送数据给客户端
      // client.send('hello socket from backend')
    })
  })
}
