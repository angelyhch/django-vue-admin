//1.创建koa实例
const Koa = require('koa')
const app = new Koa()

// 2.绑定中间件
// 2.1绑定第一层中间件
const respDurationMiddleware = require('./middleware/koa_response_duration')
app.use(respDurationMiddleware)
// 2.2绑定第二层中间件
const respHeader = require('./middleware/koa_response_header')
app.use(respHeader)
// 2.3绑定第三层中间件
const respDate = require('./middleware/koa_response_data')
app.use(respDate)

// 3.绑定8888端口号
app.listen(8888)

// 引入websocketservice文件
const WebSocketService = require('./service/web_socket_service')

// 监听websocket端口
WebSocketService.listen()
